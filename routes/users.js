/**
 * Created by jadeljerdy1 on 5/25/17.
 */

var express = require('express');
var router = express.Router();
var Users = require('../models/users.js');

/***************** * ******************/
/********** RESTFUL ROUTES ***********/
/***************** * ****************/

// Get all users
router.get('/', function(req,res) {
    Users.find({}).lean().exec(function(err, users){
        if (err)
            res.send(err);

        res.json(users)

    })
});

// Get user by Id
router.get('/:id', function(req,res){
    var id = req.params.id;
    Users.findOne({_id: id}).lean().exec(function(err,user){
        if (err)
            res.send(err)

        res.json(user);
    })
});

// Create new user.
router.post('/', function(req,res){
    var user = req.body.user;
    var newUser = new Users(user);

    newUser.save(function(err){
        if (err)
            res.send(err);

        res.json(newUser);
    });
})

// Signin
router.post('/signin', function(req,res){
    console.log("signin")
    var credentials = req.body.credentials;

    //Todo: Add JWT
    Users.findOne({email:credentials.email}).exec(function(err,user){
        if (err)
            res.send(err)

        if (!user){
            res.json({failure:"email not found"})
        } else {

            if (credentials.password === user.password ) {
                res.json({success:"user signed in"});
            } else {
                res.json({failure:"wrong pass"})
            }
        }

    })
});


module.exports = router;