/**
 * Created by jadeljerdy1 on 5/25/17.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = new Schema({
    fullName: String,
    phone: String,
    dateOfBirth: Date,
    singupDate: Date,
    email: String,
    password: String
});

module.exports = mongoose.model('User', UserSchema);